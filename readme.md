## Steps to deploy:
- `npm i`
- update your creds in .env
- `sls deploy`

## SQS-DLQ-ALARM Template:
 This repository contains the template to configure the following:
 * Allows SQS full configuration
 * Sets up DLQ for the source queue
 * Sets a lambda function to be triggered by SQS queue. 
 * Trigger has batching and its setup for partial response failure.
 * An SNS topic is created
 * Subscription of email is created
 * An Alarm is created for that SNS topic and whenever the ALARM is On an email notification is sent.
  