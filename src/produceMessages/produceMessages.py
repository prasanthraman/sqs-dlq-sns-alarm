import boto3
import json
import os
sqs = boto3.resource('sqs')

STAGE = os.environ['STAGE']
NAME = os.environ['NAME']

def lambda_handler(event,context):
    number_of_messages = event.get('n',50)
    for i in range(int(number_of_messages)):
        input_event = {'x':i}
        message_body = json.dumps(input_event)
        queue = sqs.get_queue_by_name(QueueName=f'process-queue-{NAME}-{STAGE}')
        queue.send_message(MessageBody = message_body , DelaySeconds=0)

         
    return {
        'statusCode':200,
        'body': f'pushed {number_of_messages} to source queue '
    }