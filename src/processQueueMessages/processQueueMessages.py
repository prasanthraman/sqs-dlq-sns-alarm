import json
import boto3
import os

lambda_client = boto3.client('lambda')
NAME = os.environ['NAME']
STAGE = os.environ['STAGE'] 
class MyException(Exception):
    pass
def lambda_handler(event,context):
    print(event)

    batch_item_failures = []
    sqs_batch_response = {}
    
    for record in event["Records"]:
        try:
            # process message
            message = json.loads(record['body'])
            x = int(message.get('x',0))
            if x >=25:
                raise MyException("Need to handle cases above 25")
            else:
                pass

        except Exception as e:
            batch_item_failures.append({"itemIdentifier": record['messageId']})
    if len(batch_item_failures)>0:
        input_event = {'failedMessages': batch_item_failures, 'subject': f'{len(batch_item_failures)} out of {len(event["Records"])} Failed','message': f'Some messages has been Failed. \n {batch_item_failures}'}
        lambda_client.invoke(FunctionName=f'send-notification-{NAME}-{STAGE}', InvocationType = 'Event', Payload = json.dumps(input_event))  
    sqs_batch_response["batchItemFailures"] = batch_item_failures
    return sqs_batch_response
    
