import boto3
import os

# Create an SNS client
sns = boto3.client('sns')

# Define the ARN (Amazon Resource Name) of your SNS topic
TOPIC_ARN = os.environ['TOPIC_ARN']

def lambda_handler(event,context):
    # Define the message you want to publish
    subject = event.get('subject','Batch Execution failed')
    message = event.get('message', 'Hello, this is a test message!')

    try:
        # Publish the message to the SNS topic
        response = sns.publish(
            TopicArn=TOPIC_ARN,
            Message=message,
            Subject=subject
        )
        # Print the message ID if publishing was successful
        print("Message published successfully:", response['MessageId'])
        return {
            'statusCode':200,
            'body': 'Successfully passed'
        }
    except Exception as e:
        # Handle any errors that occur during message publishing
        print("Error publishing message:", str(e))